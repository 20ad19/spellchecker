const fs = require('fs')
const { convert } = require("html-to-text");
const { removeStopwords } = require('stopword')
const jsonResult = fs.readFileSync("./dicts.json", {
    encoding: "utf8",
    flag: "r",
});

var json_dict = JSON.parse(jsonResult);
const start = Date.now();
const Dictionary = require("en-dictionary").default;
var Typo = require('typo-js-ts').Typo;
var dict = new Typo("en_US");
const html = fs.readFileSync("./cons.htm", {
    encoding: "utf8",
    flag: "r",
});

const text = convert(html, {
    wordwrap: 130,
});

//console.log(text.length)

let arr = text.replace(/\b((?:[a-z][\w-]+:(?:\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))/g, '').split(' ')
const newString = removeStopwords(arr)
    //console.log(newString.length)

let set_word = new Set(newString)

let ans = Array.from(set_word);
//let ans = [set_word]
// console.log(ans)


let words = {}
for (let i of set_word) {
    //console.log()            
    // break
    if (i in json_dict) {
        // console.log(i)
        //console.log(typeof a[i])

        // if(set_word includes[])
        if (ans.includes(json_dict[i])) {
            // console.log(a[i])
            words[i] = json_dict[i]
        }

    }
}
console.log(words)
const stop = Date.now();
console.log(`Time Taken to execute = ${(stop - start) / 1000} seconds`);