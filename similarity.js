var stringSimilarity = require("string-similarity");

const { convert } = require("html-to-text");
const fs = require("fs");
const start = Date.now();

const html = fs.readFileSync("./cons.htm")
const text = convert(html, {
    wordwrap: 130,
});

let a = text.split(/[,\s\n]/);

for (let i = 0; i < a.length; i++) {
    for (let j = i + 1; j < a.length; j++) {
        var similarity = stringSimilarity.compareTwoStrings(a[i], a[j]);
        if (similarity < 1 && similarity >= 0.7) {
            var word = {
                word1: a[i],
                index1: i,

                words2: a[j],
                index2: j

            }
            console.log(word)

        }
    }
}
// var matches = stringSimilarity.findBestMatch(similarity);