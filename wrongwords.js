var Typo = require("typo-js");
var dictionary = new Typo("en_US");

const start = Date.now();

const wordnet = require("en-wordnet").default;
const { removeStopwords } = require('stopword')

const Dictionary = require("en-dictionary").default;

var Typo = require('typo-js-ts').Typo;
var dict = new Typo("en_US");
const { convert } = require("html-to-text");
const fs = require("fs");
const { PassThrough } = require("stream");


const html = fs.readFileSync("./cons.htm")


const text = convert(html, {
    wordwrap: 130,
});

// console.log(text.length)

let arr1 = text.replace(/\b((?:[a-z][\w-]+:(?:\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))|[0-9]/g, '')
let arr2 = arr1.split(' ')

let arr = arr1.split('')
const newString = removeStopwords(arr)
const newString1 = removeStopwords(arr2)
let set_word1 = new Set(newString1)

let set_word = new Set(newString)
let ans = [set_word1]
    // console.log(ans)
let word1 = Array.from(set_word1);
let right = []

let word = Array.from(set_word);
const spell = require('spell-checker-js')

// Load dictionary
spell.load('en')

// Checking text
const check = spell.check(arr1)

// console.log(check)


let wrong = []
    //let right = []

for (let i of check) {
    var is_spelled = dictionary.check(i);
    if (is_spelled == false) {
        wrong.push(i)

    }

}

for (let i of word1) {
    var is_spelled = dictionary.check(i);
    if (is_spelled == true) {
        right.push(i)

    }

}
for (let i of wrong) {
    for (let j of right) {
        if (i.length == j.length) {
            let a = i.split('').sort();
            let b = j.split('').sort();

            if (a.every(function(u, i) {
                    return u === b[i];
                })) {
                console.log('str1 : ' + i + '   ' + 'str2 : ' + j);
            }
        }
    }
}



const stop = Date.now();

console.log(`Time Taken to execute = ${(stop - start) / 1000} seconds`);